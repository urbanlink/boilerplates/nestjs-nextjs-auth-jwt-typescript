import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { EmailService } from './email.service';

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        transport: {
          host: config.get('email.host') || 'smtp.mailtrap.io',
          port: config.get('email.port') || 2525,
          auth: {
            user: config.get('email.user') || '04401d3f37e157',
            pass: config.get('email.pass') || 'f928d33dfe6857',
          },
        },
        defaults: {
          from: config.get('email.from') || '"nest-modules" <modules@nestjs.com>',
        },
        template: {
          dir: './templates',
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
    }),
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
