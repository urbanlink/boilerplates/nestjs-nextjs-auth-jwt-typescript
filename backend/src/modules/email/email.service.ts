import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { SendEmailDto } from './email.dto';

@Injectable()
export class EmailService {
  constructor(private readonly mailer: MailerService) {}

  public async sendMail(sendOptions: SendEmailDto): Promise<any> {
    sendOptions.to = 'eb2f50fc72-63dde2@inbox.mailtrap.io';

    await this.mailer.sendMail(sendOptions);
  }
}
