import { IsNotEmpty, IsString } from 'class-validator';

export class SendEmailDto {
  to: string;

  @IsNotEmpty()
  @IsString()
  subject: string;

  @IsString()
  template?: string;

  context?: any;
}
