import { Injectable, Logger, UnprocessableEntityException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { User } from '../../user/entity/user.entity';
import { SignOptions, TokenExpiredError } from 'jsonwebtoken';
import { RefreshToken } from '../entity/refresh-token.entity';
import { TokenRepository } from '../repository/token.repository';
import { UserService } from '../../user/service/user.service';
import { InjectRepository } from '@nestjs/typeorm';

export interface RefreshTokenPayload {
  jti: number;
  sub: number;
}

@Injectable()
export class TokenService {
  private readonly logger = new Logger('Token Service');

  public constructor(
    @InjectRepository(TokenRepository)
    private readonly tokenRepository: TokenRepository,
    private readonly config: ConfigService,
    private readonly jwt: JwtService,
    private readonly users: UserService,
  ) {}

  public async generateAccessToken(user: User): Promise<string> {
    const opts: SignOptions = {
      issuer: this.config.get('jwt.issuer'),
      audience: this.config.get('jwt.audience'),
      subject: String(user.id),
    };
    return this.jwt.signAsync({}, opts);
  }

  public async generateRefreshToken(user: User, expiresIn: number): Promise<string> {
    this.logger.log(`Generate refresh token user: ${user.id}, expires: ${expiresIn}`);
    const token = await this.tokenRepository.createRefreshToken(user, expiresIn);

    const opts: SignOptions = {
      issuer: this.config.get('jwt.issuer'),
      audience: this.config.get('jwt.audience'),
      expiresIn,
      subject: String(user.id),
      jwtid: String(token.id),
    };

    return this.jwt.signAsync({}, opts);
  }

  public async resolveRefreshToken(encoded: string): Promise<{ user: User; token: RefreshToken }> {
    const payload = await this.decodeRefreshToken(encoded);
    const token = await this.getStoredTokenFromRefreshTokenPayload(payload);

    if (!token) {
      throw new UnprocessableEntityException('Refreh token not found');
    }
    if (token.is_revoked) {
      throw new UnprocessableEntityException('Refresh token is revoked');
    }

    const user = await this.getUserFromRefreshTokenPayload(payload);

    if (!user) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return { user, token };
  }

  public async createAccessTokenFromRefreshtoken(refreshToken: string): Promise<{ accessToken: string; user: User }> {
    this.logger.log('Create access token from refresh token');
    const { user } = await this.resolveRefreshToken(refreshToken);
    const accessToken = await this.generateAccessToken(user);

    return { user, accessToken };
  }

  // --- PRIVATE METHODS --- //

  private async getUserFromRefreshTokenPayload(payload: RefreshTokenPayload): Promise<User | null> {
    const subId = payload.sub;

    if (!subId) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return this.users.findById(subId);
  }

  private async getStoredTokenFromRefreshTokenPayload(payload: RefreshTokenPayload): Promise<RefreshToken | null> {
    const tokenId = payload.jti;

    if (!tokenId) {
      throw new UnprocessableEntityException('Refresh token malformed');
    }

    return this.tokenRepository.findTokenById(tokenId);
  }

  private async decodeRefreshToken(token: string): Promise<RefreshTokenPayload> {
    try {
      return await this.jwt.verifyAsync(token);
    } catch (e) {
      if (e instanceof TokenExpiredError) {
        throw new UnprocessableEntityException('Refresh token expired');
      } else {
        throw new UnprocessableEntityException('Refresh token malformed');
      }
    }
  }
}
