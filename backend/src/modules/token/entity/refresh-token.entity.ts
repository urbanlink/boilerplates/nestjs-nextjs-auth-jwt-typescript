import { Expose } from 'class-transformer';
import { User } from '../../user/entity/user.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class RefreshToken {
  @PrimaryGeneratedColumn()
  @Expose()
  id: string;

  @Column({ default: false })
  is_revoked: boolean;

  @Column()
  expires: Date;

  @ManyToOne(() => User, (user: User) => user.refresh_tokens)
  @JoinColumn({ name: 'user_id' })
  user: User;
}
