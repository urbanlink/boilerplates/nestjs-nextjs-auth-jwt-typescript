import { Logger } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { UserDto } from '../../user/dto/user.dto';
import { RefreshToken } from '../entity/refresh-token.entity';

@EntityRepository(RefreshToken)
export class TokenRepository extends Repository<RefreshToken> {
  private readonly logger = new Logger('TokenRepository');

  constructor() {
    super();
    this.logger.log('Refresh token repository constructor');
  }

  public async createRefreshToken(user: UserDto, ttl: number): Promise<RefreshToken> {
    const expiration = new Date();
    expiration.setTime(expiration.getTime() + ttl);

    const refreshToken = this.create({
      user,
      is_revoked: false,
      expires: expiration,
    });

    await this.save(refreshToken);

    return refreshToken;
  }

  public async findTokenById(id: number): Promise<RefreshToken | null> {
    return this.findOne({ where: { id } });
  }
}
