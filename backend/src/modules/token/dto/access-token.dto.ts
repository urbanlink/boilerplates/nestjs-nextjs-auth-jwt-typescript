export class AccessTokenDto {
  expiresIn: string;
  accessToken: string;
}
