import { IsString } from 'class-validator';

export class RefreshTokenDto {
  @IsString()
  expiresIn: string;

  @IsString()
  refreshToken?: string;
}

export class RefreshTokenRequestDto {
  @IsString()
  refresh_token: string;
}
