import {
  Body,
  Controller,
  Get,
  Logger,
  NotFoundException,
  Post,
  Req,
  UnauthorizedException,
  UnprocessableEntityException,
  UseGuards,
} from '@nestjs/common';
import { UserService } from '../..//user/service/user.service';
import { TokenService } from '../../token/service/token.service';
import { ConfigService } from '@nestjs/config';
import { User } from '../../user/entity/user.entity';
import { RefreshTokenRequestDto } from '../../token/dto/refresh-token.dto';
import { RegistrationStatusDto } from '../dto/registration-status.dto';
import { JWTGuard } from '../guard/jwt.guard';
import { ApiTags } from '@nestjs/swagger';
import { UserRegisterDto } from '../../user/dto/user-register.dto';
import { UserLoginDto } from '../../user/dto/user-login.dto';
import { VerifyEmailDto } from '../dto/verify-email.dto';
import { ForgotPasswordDto } from '../dto/forgot-password.dto';
import { SetPasswordDto } from '../dto/set-password.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { SendEmailVerificationRequestDto } from '../dto/send-email-verification';

export interface AuthenticationPayload {
  user: Partial<User>;
  jwt: {
    type: string;
    token: string;
    refresh_token?: string;
  };
}

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  private readonly logger = new Logger('AuthController');

  constructor(
    private readonly userService: UserService,
    private readonly tokenService: TokenService,
    private readonly config: ConfigService,
    private readonly mailer: MailerService,
  ) {}

  /**
   *
   * Register a new user (return new user)
   * After registration a verification email is send
   *
   **/
  @Post('/register')
  public async register(@Body() body: UserRegisterDto): Promise<RegistrationStatusDto> {
    this.logger.log('Register a new user account');
    const user = await this.userService.createUserFromRequest(body);

    return {
      success: true,
      user: {
        id: user.id,
      },
    };
  }

  @Post('/login')
  public async login(@Body() body: UserLoginDto) {
    const { email, password } = body;

    const user = await this.userService.findByEmail(email);
    if (!user) {
      this.logger.log(`User not found ${email}`);
      throw new NotFoundException({
        error_status: 'user_not_found',
        error_message: 'User not found',
        email,
      });
    }
    // Validate account status
    if (!user.email_verified) {
      this.logger.log(`User primary email not verified ${user.id}`);
      throw new UnprocessableEntityException({
        error_status: 'not_verified',
        error_message: `This account is not yet verified, validate your email address. <a href="/user/send-email-verification?email=${user.email}">Click here to resend the verification email</a>.`,
        email,
      });
    }
    if (!user.enabled) {
      this.logger.log({
        error_status: 'not_enabled',
        error_message: `User account not enabled ${user.id}`,
      });
      throw new UnauthorizedException('This account is disabled');
    }

    // Validate password
    const valid = user ? await this.userService.validateCredentials(user, password) : false;
    if (!valid) {
      throw new UnauthorizedException({
        error_status: 'invalid_password',
        error_message: `The supplied password is incorrect`,
        email: user.email,
      });
    }

    // Create JWT tokens
    const token = await this.tokenService.generateAccessToken(user);
    const refresh = await this.tokenService.generateRefreshToken(user, this.config.get('jwt.refreshTokenTTL'));
    const payload = this.buildResponsePayload(user, token, refresh);

    return {
      status: 'success',
      data: payload,
    };
  }

  @Post('/refresh')
  async refresh(@Body() body: RefreshTokenRequestDto) {
    const { refresh_token } = body;
    const { user, accessToken } = await this.tokenService.createAccessTokenFromRefreshtoken(refresh_token);
    this.logger.log(`New refreshtoken created for ${user.id}: ${accessToken}`);
    const payload = this.buildResponsePayload(user, accessToken);

    return {
      status: 'success',
      data: payload,
    };
  }

  @Get('/me')
  @UseGuards(JWTGuard)
  public async getUser(@Req() request) {
    const userId = request.user.id;

    const user = await this.userService.findById(userId);

    return {
      status: 'success',
      data: user,
    };
  }

  // Resend Email Verification mail
  @Post('/send-email-verification')
  public async resendVerification(@Body() body: SendEmailVerificationRequestDto) {
    this.logger.log('Resend verification email token');
    const { email } = body;
    this.logger.log('email' + email);
    const user = await this.userService.findByEmail(email);
    if (!user) throw new NotFoundException('user not found');

    const result = await this.userService.sendEmailVerificationCode(user);

    return {
      status: result,
      message: `Verification email sent to ${user.email}`,
    };
  }

  // Confirm Email address with verification code
  @Post('/verify-email')
  public async verifyEmail(@Body() body: VerifyEmailDto) {
    const { token, email } = body;
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new NotFoundException('User not found');
    }

    const result = await this.userService.verifyEmailAddress(user, token);

    return {
      status: result.email_verified,
      message: `Account verified for user ${user.id}`,
    };
  }

  // Request new password
  @Post('/forgot-password')
  public async newPassword(@Body() body: ForgotPasswordDto) {
    const { email } = body;
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new NotFoundException('user not found');
    }

    const result = await this.userService.requestNewPassword(user);

    return {
      status: result,
      message: `New password link sent to ${user.id}`,
    };
  }

  // Request new password
  @Post('/set-password')
  public async setPassword(@Body() body: SetPasswordDto) {
    const { token, password, email } = body;
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new NotFoundException('user not found');
    }

    const result = await this.userService.setPassword(user, token, password);

    return {
      status: result,
      message: `New password link sent to ${user.id}`,
    };
  }

  // Set new password
  private buildResponsePayload(user: User, accessToken: string, refreshToken?: string): AuthenticationPayload {
    return {
      user: {
        id: user.id,
        username: user.username,
        email: user.email,
      },
      jwt: {
        type: 'bearer',
        token: accessToken,
        ...(refreshToken ? { refresh_token: refreshToken } : {}),
      },
    };
  }
}
