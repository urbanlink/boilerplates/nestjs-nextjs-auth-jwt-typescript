// import { Injectable, UnauthorizedException } from '@nestjs/common';
// import { CreateUserDto } from 'src/modules/user/dto/user.create.dto';
// import { LoginUserDto } from 'src/modules/user/dto/user.login.dto';
// import { UserService } from 'src/modules/user/service/user.service';
// import { LoginStatus } from '../dto/login-status.dto';
// import { RegistrationStatusDto } from '../dto/registration-status.dto';

// @Injectable()
// export class AuthService {
//   constructor(private readonly userService: UserService) {}

//   // Register a new user account
//   async register(userDto: CreateUserDto): Promise<RegistrationStatusDto> {
//     let status: RegistrationStatusDto = {
//       success: true,
//       message: 'user registered',
//     };
//     try {
//       await this.userService.createU(userDto);
//     } catch (err) {
//       status = {
//         success: false,
//         message: err,
//       };
//     }

//     return status;
//   }

//   // Login a user
//   async login(loginUserDto: LoginUserDto): Promise<LoginStatus> {
//     const user = await this.userService.findByLogin(loginUserDto);
//     // Generate and sign access and refresh tokens
//     const token = this.issueTokens(user);

//     return {
//       username: user.username,
//       ...token,
//     };
//   }
// }
