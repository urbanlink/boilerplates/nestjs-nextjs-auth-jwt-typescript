export class LoginStatus {
  username: string;
  accessToken?: string;
}
