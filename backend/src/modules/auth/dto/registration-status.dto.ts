export interface RegistrationStatusDto {
  success: boolean;
  message?: string;
  user: {
    id: string;
  };
}
