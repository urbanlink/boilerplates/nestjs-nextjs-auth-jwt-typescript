import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { AuthController } from './controller/auth.controller';
import { PassportModule } from '@nestjs/passport';
import { TokenModule } from '../token/token.module';
import { JWTGuard } from './guard/jwt.guard';

@Module({
  imports: [PassportModule.register({ defaultStrategy: 'jwt' }), UserModule, TokenModule],
  controllers: [AuthController],
  providers: [JWTGuard],
})
export class AuthModule {}
