import { IsNotEmpty } from 'class-validator';

export class UserLoginDto {
  @IsNotEmpty({ message: 'An email is required' })
  readonly email: string;

  @IsNotEmpty({ message: 'A password is required' })
  readonly password: string;
}
