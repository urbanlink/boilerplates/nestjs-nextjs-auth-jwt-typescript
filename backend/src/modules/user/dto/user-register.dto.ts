import { IsNotEmpty, MinLength } from 'class-validator';

export class UserRegisterDto {
  @IsNotEmpty({ message: 'An email is required' })
  readonly email: string;

  @IsNotEmpty({ message: 'A password is required' })
  @MinLength(6, { message: 'Your password must be at least 6 characters' })
  readonly password: string;

  @IsNotEmpty({ message: 'A username is required' })
  @MinLength(3, { message: 'Your username must be at least 3 characters' })
  readonly username: string;
}
