import * as crypto from 'crypto';
import { BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Expose } from 'class-transformer';
import { RefreshToken } from '../../token/entity/refresh-token.entity';

@Entity()
export class User {
  @Expose()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Expose()
  @Column({ nullable: false, unique: true })
  username: string;

  @Expose()
  @Column({ nullable: false, unique: true })
  email: string;

  @Column({ nullable: false })
  password: string;

  @Column({ nullable: true })
  password_reset_sent: Date;

  @Column({ nullable: true })
  password_reset_code: string;

  @Column({ nullable: false })
  salt: string;

  @Expose()
  @Column({ default: false })
  enabled: boolean;

  @Column({ default: false })
  email_verified: boolean;

  @Column({ nullable: true })
  email_verification_code: string;

  @Column({ nullable: true })
  email_verification_sent: Date;

  @OneToMany(() => RefreshToken, (refreshtoken) => refreshtoken.user)
  refresh_tokens: RefreshToken[];

  // Hash password before inserting a new user
  @BeforeInsert()
  async hashPassword() {
    // Creating a unique salt for a particular user
    this.salt = crypto.randomBytes(16).toString('hex');
    // Hashing user's salt and password with 1000 iterations,
    this.password = crypto.pbkdf2Sync(this.password, this.salt, 1000, 64, `sha512`).toString(`hex`);
  }
}
