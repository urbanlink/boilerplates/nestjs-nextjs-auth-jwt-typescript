import {
  Injectable,
  Logger,
  NotAcceptableException,
  PreconditionFailedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import * as crypto from 'crypto';
import { User } from '../entity/user.entity';
import { UserRepository } from '../repository/user.repository';
import { UserRegisterDto } from '../dto/user-register.dto';
import { EmailService } from 'src/modules/email/email.service';

@Injectable()
export class UserService {
  private readonly logger = new Logger('UserService');

  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    private readonly config: ConfigService,
    private readonly mailerService: EmailService,
  ) {}

  public async validateCredentials(user: User, password: string): Promise<boolean> {
    const hash = crypto.pbkdf2Sync(password, user.salt, 1000, 64, `sha512`).toString(`hex`);
    return user.password === hash;
  }

  public async createUserFromRequest(request: UserRegisterDto): Promise<User> {
    const { email, password, username } = request;
    this.logger.log(`CreateUserFromRequest ${email}, ${password}, ${username}`);

    const existingEmail = await this.findByEmail(email);
    if (existingEmail) {
      throw new UnprocessableEntityException('Email already exists');
    }

    const existingUsername = await this.findByUsername(username);
    if (existingUsername) {
      throw new UnprocessableEntityException({
        email,
        error_code: 'username_taken',
        error_message: `The username ${username} is already taken, please choose a different one`,
      });
    }
    this.logger.debug('Create new account');
    const user = await this.userRepository.createUser({ email, password, username });

    // Send email confirm email
    this.sendEmailVerificationCode(user);

    return user;
  }

  public async findById(id: number): Promise<User | null> {
    return this.userRepository.findById(id);
  }

  public async findByUsername(username: string): Promise<User | null> {
    this.logger.log(`Find by username ${username}`);
    return this.userRepository.findByUsername(username);
  }

  public async findByEmail(email: string): Promise<User | null> {
    this.logger.log(`Find by email ${email}`);
    return await this.userRepository.findByEmail(email);
  }

  /**
   * Send a new email verification code to the email address for a given user.
   *
   * @param user
   * @returns boolean
   */
  public async sendEmailVerificationCode(user: User): Promise<boolean> {
    this.logger.log(`Send new email verification code for user: ${user.id}`);
    if (user.email_verified) {
      this.logger.log('email address already verified');
      throw new UnprocessableEntityException({
        error_code: 'already_verified',
        error_message: 'Email address is already confirmed',
      });
    }
    if (user.email_verification_sent) {
      const diff = new Date(user.email_verification_sent.getTime() + this.config.get('sendVerifyEmailTTL'));
      if (diff > new Date()) {
        throw new UnprocessableEntityException({
          error_code: 'limit_reached',
          error_message: `Send limit reached, please try again in ${diff}`,
        });
      }
    }

    user.email_verification_code = crypto.randomBytes(16).toString('hex');
    user.email_verification_sent = new Date();

    try {
      await this.mailerService.sendMail({
        to: user.email,
        subject: 'Please validate your account',
        template: './validate',
        context: {
          link:
            this.config.get('domain.frontend') +
            `/user/validate-email-verification?email=${user.email}&token=${user.email_verification_code}`,
          token: user.email_verification_code,
          email: user.email,
          username: user.username,
        },
      });
    } catch (err) {
      console.warn(err);
    }

    try {
      await this.userRepository.save(user);
    } catch (err) {
      console.warn(err);
    }

    this.logger.log(`Email verification code for user: ${user.id} sent`);
    return true;
  }

  /**
   *
   * @param user User
   * @param token string
   * @returns boolean
   */
  public async verifyEmailAddress(user: User, token: string): Promise<User> {
    // Verify code
    if (!token) {
      throw new UnprocessableEntityException({ error_code: 'code_empty', error_message: 'Code is empty' });
    }
    if (user.email_verified) {
      throw new UnprocessableEntityException({
        error_code: 'email_verified',
        error_message: 'Email address is already confirmed',
      });
    }
    if (user.email_verification_code !== token) {
      throw new UnprocessableEntityException({ error_code: 'code_invalid', error_message: 'Code is not valid' });
    }
    if (new Date(user.email_verification_sent.getTime() + this.config.get('verifyEmailTokenTTL')) < new Date()) {
      throw new UnprocessableEntityException({ error_code: 'code_expired', error_message: 'Code expired' });
    }
    // Update user
    user.email_verification_code = null;
    user.email_verification_sent = null;
    user.email_verified = true;
    // Enable user when email is confirmed
    user.enabled = true;
    await this.userRepository.save(user);

    // send confirmation email to user
    this.mailerService.sendMail({
      to: user.email,
      subject: 'Account verified',
      template: 'account_verified',
    });

    return user;
  }

  public async requestNewPassword(user: User): Promise<any> {
    // Verification
    if (!user.email_verified) {
      throw new PreconditionFailedException('Email address is not yet confirmed');
    }
    if (!user.enabled) {
      throw new PreconditionFailedException('User account is not yet enabled');
    }
    if (new Date(user.password_reset_sent.getTime() + this.config.get('requestNewPasswordTTL')) < new Date()) {
      throw new NotAcceptableException('Password request limit reached');
    }

    // set and send code
    user.password_reset_code = crypto.randomBytes(16).toString('hex');
    user.password_reset_sent = new Date();
    await this.userRepository.save(user);

    return user;
  }

  public async setPassword(user: User, token: string, password: string): Promise<any> {
    // Verification
    if (!user.email_verified) {
      throw new PreconditionFailedException('Email address is not yet confirmed');
    }
    if (!user.enabled) {
      throw new PreconditionFailedException('User account is not yet enabled');
    }
    console.log(token, password);

    return true;
  }
}
