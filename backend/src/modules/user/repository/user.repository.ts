/**
 *
 * one to find a user by their ID for generating refresh tokens,
 * one to find them by their username at login,
 * and a final method to create a new user for registration.
 *
 **/

import { HttpException, HttpStatus, Logger } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateUserDto } from '../dto/user-create.dto';
import { User } from '../entity/user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  private readonly logger = new Logger('UserRepository');

  constructor() {
    super();
    this.logger.log('user repository');
  }

  public async findById(id: number): Promise<User | null> {
    return this.findOne(id);
  }

  public async findByUsername(username: string): Promise<User | null> {
    this.logger.log(`Find by username ${username}`);
    return this.findOne({ where: { username } });
  }

  public async findByEmail(email: string): Promise<User | null> {
    return this.findOne({ where: { email } });
  }

  public async createUser(userDto: CreateUserDto): Promise<User> {
    const { username, password, email } = userDto;

    const nameExists = await this.findOne({ where: { username } });
    if (nameExists) {
      throw new HttpException('Username already exists', HttpStatus.BAD_REQUEST);
    }

    const emailExists = await this.findOne({ where: { email } });
    if (emailExists) {
      throw new HttpException('Email already exists', HttpStatus.BAD_REQUEST);
    }

    const user = this.create({
      username,
      password,
      email,
    });

    await this.save(user);

    return user;
  }
}
