import 'reflect-metadata';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger('Main', true);
  const globalPrefix = 'v1';

  app.enableCors();
  app.use(helmet());
  app.setGlobalPrefix(globalPrefix);

  // Build the swagger doc only in dev mode
  if (AppModule.isDev) {
    logger.log('Start Swagger UI');
    const swaggerOptions = new DocumentBuilder()
      .setTitle('App name')
      .setDescription('API documentation for NestJS Auth backend')
      .setVersion(globalPrefix)
      // .addBearerAuth('Authorization', 'header')
      .build();

    const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions);

    SwaggerModule.setup(`${globalPrefix}/swagger`, app, swaggerDoc);
  }

  // Ensure all endpoints are protected to receive the right data using class-validator
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  await app.listen(8000);
}

bootstrap();
