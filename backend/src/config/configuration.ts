import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import * as Joi from 'joi';

export const configSchema = Joi.object({
  NODE_ENV: Joi.string().valid('development', 'ci', 'staging', 'production').default('production'),
  PORT: Joi.number().port().default(3000),
  GLOBAL_PREFIX: Joi.string().default('v1'),
  CORS_ORIGIN: Joi.string().default('localhost'),

  DATABASE_HOST: Joi.string().default('localhost'),
  DATABASE_PORT: Joi.number().port().default(5432),

  AUTH_SECRET: Joi.string().required(),
  AUTH_TOKEN_LIFETIME: Joi.alternatives(Joi.string(), Joi.number()),
  AUTH_REFRESH_TOKEN_LIFETIME: Joi.alternatives(Joi.string(), Joi.number()),
});

export interface Config {
  env: string;
  port: number;
  globalPrefix: string;
  cors: CorsOptions;
  domain: {
    frontend: string;
    backend: string;
  };
  db: {
    type: string;
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
    entities: string[];
    synchronize: boolean;
  };
  jwt: {
    issuer: string;
    audience: string;
    secret: string;
    accessTokenLifetime: number | string;
    refreshTokenTTL: number;
  };
  sendVerifyEmailTTL: number;
  verifyEmailTokenTTL: number;
  requestNewPasswordTTL: number;
}

export const configFactory = (): Config => {
  return {
    env: process.env.NODE_ENV || 'development',
    port: parseInt(process.env.SERVER_PORT, 10) || 8000,
    globalPrefix: process.env.API_PREFIX || 'v1',
    cors: {
      origin: process.env.CORS_ORIGIN
        ? process.env.CORS_ORIGIN.split(',').map((origin) => new RegExp(`^https?://${origin}:?[0-9]*$`))
        : false,
    },
    domain: {
      frontend: process.env.DOMAIN_FRONTEND || 'http://localhost:3000',
      backend: process.env.DOMAIN_BACKEND || 'http://localhost:8000',
    },
    db: {
      type: process.env.DATABASE_TYPE || 'postgres',
      host: process.env.DATABASE_HOST || 'db',
      port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
      username: process.env.DATABASE_USERNAME || 'postgres',
      password: process.env.DATABASE_PASSWORD || 'postgres',
      database: process.env.DATABASE_DB || 'app',
      entities: [process.env.DATABASE_ENTITIES] || ['dist/**/*.entity{.ts,.js}'],
      synchronize: process.env.DATABASE_SYNC === 'true' ? true : false || false,
    },
    jwt: {
      issuer: process.env.AUTH_ISSUER || 'authapp',
      audience: process.env.AUTH_AUDIENCE || 'authapp',
      secret: process.env.AUTH_SECRET || '1234567890qwertyuiop',
      accessTokenLifetime: parseInt(process.env.AUTH_ACCESS_TOKEN_LIFETIME, 10) || 5 * 60, // 5 minutes
      refreshTokenTTL:
        parseInt(process.env.AUTH_REFRESH_TOKEN_LIFETIME, 10) || parseInt(String(1000 * 60 * 60 * 24 * 40)), // 1 month
    },
    sendVerifyEmailTTL: parseInt(process.env.RESEND_EMAIL_TTL, 10) || 1000 * 60, // 1 minute
    verifyEmailTokenTTL: parseInt(process.env.RESEND_EMAIL_TTL, 10) || 1000 * 60 * 60 * 24 * 7, // 7 days
    requestNewPasswordTTL: parseInt(process.env.RESEND_EMAIL_TTL, 10) || 1000 * 60, // 1 minute
  };
};
