import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { configFactory, configSchema } from './config/configuration';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { TokenModule } from './modules/token/token.module';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import * as path from 'path';
import * as fs from 'fs';
import { EmailModule } from './modules/email/email.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: configSchema,
      load: [configFactory],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) =>
        ({
          type: config.get('db.type'),
          host: config.get('db.host'),
          port: +config.get('db.port'),
          username: config.get('db.username'),
          password: config.get('db.password'),
          database: config.get('db.database'),
          entities: ['dist/**/*.entity{.ts,.js}'],
          synchronize: config.get('db.synchronize'),
        } as PostgresConnectionOptions),
    }),
    EmailModule,
    UserModule,
    TokenModule,
    AuthModule,
  ],
})
export class AppModule {
  static isDev: boolean;

  constructor(private readonly config: ConfigService) {
    console.log(fs.existsSync(path.join(process.env.PWD, '/src/templates')));
    AppModule.isDev = config.get('env') !== 'production';
  }
}
