import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.NETWORK_API_URL || process.env.NEXT_PUBLIC_API_URL,
});

export default async (req,res) => {
  console.log('[API register] - Register a new account')
  if (req.method === 'POST') {
    const url = '/auth/register';
    try {
      const result = await axiosInstance.post(url, req.body)
      res.status(200).json(result.data);
    } catch(err) {
      console.log(err)
      res.status(422).json(err.response.data)
    }
  } else {
    return res.status(404);
  }
}
