import axios from 'axios'
import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

const axiosInstance = axios.create({
  baseURL: process.env.NETWORK_API_URL || process.env.NEXT_PUBLIC_API_URL,
});

/**
 * Takes a token, and returns a new token with updated
 * `accessToken` and `accessTokenExpires`. If an error occurs,
 * returns the old token and an error property
 *
 * @param  {object}  token     Decrypted JSON Web Token
 *
 */
const refreshAccessToken = async (token: any) => {
  try {
    const result = await axios.post(`${process.env.NEXT_PUBLIC_API_ENDPOINT}/auth/refresh`, token.refreshToken);
    if (result.status !== 201) throw result;

    return {
      ...token,
      accessToken: result.data.access_token,
      accessTokenExpires: Date.now() + result.data.expires_in * 1000,
      refreshToken: result.data.refresh_token ?? token.refreshToken, // Fall back to old refresh token
    };
  } catch (error) {
    return {
      error: "RefreshAccessTokenError",
    };
  }
}

const providers = [
  Providers.Credentials({
    name: 'Credentials',
    authorize: async (credentials:{email:string, password: string}) => {
      try {
        const result = await axiosInstance.post(`/auth/login`, credentials);
        return result.data
      } catch (err) {
        throw new Error(err.response?.data?.error_message+'&email='+err.response?.data?.email || 'There was an error')
      }
    }
  })
]

const callbacks = {
  /**
   * @param  {object}  token     Decrypted JSON Web Token
   * @param  {object}  user      User object      (only available on sign in)
   * @param  {object}  account   Provider account (only available on sign in)
   * @param  {object}  profile   Provider profile (only available on sign in)
   * @param  {boolean} isNewUser True if new user (only available on sign in)
   * @return {object}            JSON Web Token that will be saved
   */
  async jwt(token, data) {
    console.log('JWT callback')
    console.log(token);
    console.log(data);
    // Initial sign in
    if (data?.data) {
      const { user, jwt } = data.data
      const a = {
        accessToken: jwt.token,
        accessTokenExpires: Date.now() + jwt.expires_in * 1000,
        refreshToken: jwt.refresh_token,
        user,
      }
      console.log('a',a);
      return a;
    }

    // Return previous token if the access token has not expired yet
    if (Date.now() < token.accessTokenExpires) return token;

    return refreshAccessToken(token);
  },

  /**
   * @param  {object} session      Session object
   * @param  {object} token        User object    (if using database sessions)
   *                               JSON Web Token (if not using database sessions)
   * @return {object}              Session that will be returned to the client
   */
  async session(session, token) {
    console.log('SESSION callback', session, token);
    if (token.error) session.error = token.error;
    if (token.accessToken) session.accessToken = token.accessToken;
    if (token.refreshToken) session.refreshToken = token.refreshToken;
    if (token.user) session.user = token.user;

    return session
  }
}

const options = {
  providers,
  callbacks,
  // pages: {
  //   error: '/login' // Changing the error redirect page to our custom login page
  // }
}

export default (req, res) => NextAuth(req, res, options)
