import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.NETWORK_API_URL || process.env.NEXT_PUBLIC_API_URL,
});

export default async (req: Request, res) => {
  console.log('send email validation')
  if (req.method === 'POST') {
    const url = '/auth/send-email-verification'; 
    try {
      const result = await axiosInstance.post(url, req.body)
      console.log(result.data)
      res.status(200).json(result.data);
    } catch (err) { 
      let errorMessage='An error occured';
      if (err.response?.data) {
        errorMessage += Object.keys(err.response.data)
          .map(key => `${key}=${err.response.data[key]}`)
          .join('&');
      }
      console.log('[API register] - error data', errorMessage);
      res.status(422).json(err.response.data)
    }
  } else {
    return res.status(404);
  }
}