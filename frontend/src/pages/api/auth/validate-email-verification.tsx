import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.NETWORK_API_URL || process.env.NEXT_PUBLIC_API_URL,
});

export default async (req: Request, res) => {
  if (req.method === 'POST') {
    const url = '/auth/verify-email'; 
    try {
      const result = await axiosInstance.post(url, req.body)
      res.status(200).json(result.data);
    } catch (err) { 
      console.log(err.response)
      res.status(err.response.status).json(err.response.data);
    }
  } else {
    return res.status(404);
  }
}