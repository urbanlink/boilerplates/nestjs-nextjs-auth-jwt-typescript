import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { signIn, useSession } from 'next-auth/client'
import { Layout } from '../components/Layout'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Login () {

  const [session, loading] = useSession();

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isLoginStarted, setIsLoginStarted] = useState(false)
  const [loginError, setLoginError] = useState('')

  const router = useRouter()

  // Authentication check
  useEffect(() => {
		if(!loading && session?.accessToken)
			router.push('/')
	}, [loading, session])

  // Router params check
  useEffect(() => {
    if (router.query.email) setEmail(router.query.email.toString());
    if (router.query.error) {
      let msg:string=router.query.error?.toString() || "There was an error";
      switch (router.query.error_status) {
        case 'not_verified':
          msg=`The email address for this account is not yet verified. <a href="/user/send-email-validation?email=${router.query.email}" class="underline">Resend email validation link</a>.`;
          break;
        default:
      }
      setLoginError(msg)
    }
  }, [router])

  //
  const handleChange = e => {
    setLoginError(undefined);

    if (e.target.id === 'email')
      setEmail(e.target.value);
    if (e.target.id === 'password')
      setPassword(e.target.value);
  }

  //
  const handleLogin = (e) => {
    e.preventDefault();

    setIsLoginStarted(true);
    setLoginError(undefined);

    signIn('credentials', {
      email,
      password,
      callbackUrl: `${window.location.origin}/welcome`
    })
  }

  return (
    <Layout>
      <div className="flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <img className="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="Workflow" />
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
              Sign in to your account
            </h2>
            <p className="mt-2 text-center text-sm text-gray-600">
              Or&nbsp;
              <a href="/register" className="font-medium text-indigo-600 hover:text-indigo-500">
                register a new account
              </a>
            </p>
          </div>
          <form className="mt-8 space-y-6" onSubmit={handleLogin}>
            <div className="rounded-md shadow-sm -space-y-px">
              <div>
                <label htmlFor="email" className="sr-only">Email</label>
                <input id='email' name="email" type='email' required placeholder="Email address" value={email} onChange={handleChange} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"/>
              </div>

              <div>
                <label htmlFor="password" className="sr-only">Password</label>
                <input id='password' name="password" type='password' placeholder="Password" autoComplete="current-password" required value={password} onChange={handleChange} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"/>
              </div>
            </div>

            <div className="text-sm text-right mb-4">
              <a href="/forgot-password" className="font-medium text-indigo-600 hover:text-indigo-500">
                Forgot your password?
              </a>
            </div>

            {loginError && (
              <div role="alert" className="my-2">
                <div className="border border-red-400 rounded bg-red-100 px-4 py-3 text-red-700">
                  <div dangerouslySetInnerHTML={{__html: loginError}}/>
                </div>
              </div>
            )}

            <div>
              <button type="submit" disabled={isLoginStarted} className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                  <svg className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fillRule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clipRule="evenodd" />
                  </svg>
                </span>
                {isLoginStarted ? <FontAwesomeIcon icon={faSpinner} spin/> : 'Log in'}
              </button>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  )
}
