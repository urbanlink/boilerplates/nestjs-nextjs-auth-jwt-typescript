import { useSession } from "next-auth/client";
import React from "react";
import {Layout} from "../components/Layout";

const Home = () => {
  const [session, loading] = useSession();
  // console.log(session)

  return (
  <Layout>
    <div className="container p-4">
      <h1>NextAuth.js Example</h1>
      <p>
        This is an example site to demonstrate how to use <a href={`https://next-auth.js.org`}>NextAuth.js</a> for authentication.
      </p>
      {loading && (<p>Loading user</p>)}
      {!loading && !session && (
        <a className="btn" href="/login">Login</a>
      )}
    </div>
  </Layout>
);
  }

export default Home;