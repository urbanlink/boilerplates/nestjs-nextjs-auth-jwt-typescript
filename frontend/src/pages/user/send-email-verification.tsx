import React, { ChangeEvent, ChangeEventHandler, FormEvent, useEffect, useState } from "react";
import { useSession } from "next-auth/client";
import {Layout} from "../../components/Layout";
import { useRouter } from "next/router";
import axios from 'axios';

const SendEmailValidation = () => {
  const router = useRouter();

  const [session, loading] = useSession();
  const [email, setEmail] = useState<string>('');
  const [error, setError] = useState<string>();
  const [submitting, setSubmitting] = useState<boolean>(false);
  const [success, setSuccess] = useState<boolean>(false); 

  // Authentication check
  useEffect(() => {
		if(!loading && session?.accessToken) {
      console.log('already logged in');
			router.push('/')
    }
	}, [loading, session])
  
  // Router params check 
  useEffect(() => {
    if (router.query.email) {
      setEmail(router.query.email.toString());
    }
  }, [router])

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setError(undefined);
    setEmail(e.target.value);
  }
  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setSubmitting(true);
    setError(undefined);
    try {
      const result = await axios.post(`/api/auth/send-email-verification`, {email});
      console.log('submit result', result.data); 
      if (result.data.status === true) {
        setSuccess(true);
      }
    } catch(err) {
      setError(err.response.data.error_message);
    } finally {
      setSubmitting(false);
    }
  }

  return (
  <Layout>
    <div className="container p-4">
      <div className="flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <a href="\"><img className="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="Workflow" /></a>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Resend email validation link</h2>
            <p className="mt-3 md:mx-10 text-center text-sm text-gray-600">Enter your email address and we'll send you a link to confirm your account</p>
          </div>
          <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
            <div className="rounded-md shadow-sm">
              <div>
                <label htmlFor="email" className="sr-only">Email</label>
                <input id='email' name="email" type='email' required placeholder="Email address" value={email} onChange={handleChange} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"/>
              </div>
            </div>
            <div>
              <button type="submit" disabled={!email || submitting} className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                {submitting ? '...' : 'Send me a new email validation link'}
              </button>
            </div>
            
            {error && (
              <div role="alert" className="my-2">
                <div className="border border-red-400 rounded bg-red-100 px-4 py-3 text-red-700">
                  <div dangerouslySetInnerHTML={{__html: error}}/>
                </div>
              </div>
            )}

            
            <p className="mt-2 text-center text-sm text-gray-600">
            or&nbsp;<a href="/login" className="font-medium text-indigo-600 hover:text-indigo-500">
                go to the login page
              </a>
            </p>
          </form>
        </div>
      </div>
    </div>
    {success && (
      <div className="modal fixed w-full h-full top-0 left-0 flex items-center justify-center">
        <div className="modal-overlay absolute w-full h-full bg-white opacity-95"></div>
        <div className="modal-container fixed w-full h-full z-50 overflow-y-auto ">
          <div className="modal-content max-w-screen-md container mx-auto h-auto text-left p-4">
          <div className="pb-2 pt-20 text-center">
            <p className="text-2xl font-bold">Verification email sent</p>
            <p>We have sent an email with a confirmation code to {email}. Please check your inbox and follow the link to verify your account.</p>
            <a href="/" className="mt-8 group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Back to homepage</a>
          </div>
          </div>
        </div>
      </div>
    )}
  </Layout>
);
  }

export default SendEmailValidation;