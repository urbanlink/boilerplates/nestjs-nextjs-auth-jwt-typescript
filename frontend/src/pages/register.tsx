import { useState, useEffect, FormEvent } from 'react'
import axios, { AxiosError } from 'axios';
import { useRouter } from 'next/router'
import { Layout } from '../components/Layout'

export default function Register () {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [submitting, setSubmitting] = useState(false);
  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');
  const router = useRouter()

  useEffect(() => {
    console.log('req.query', router.query)
    if (router.query.error) {
      setError(router.query.error?.toString() || undefined)
      setEmail(router.query.email?.toString() || undefined)
    }
  }, [router])

  const handleRegister = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setSubmitting(true)
    console.log('handleRegister', email, password, username)
    try {
      const result = await axios.post(`/api/auth/register`, {email, password, username});
      console.log('register result', result.data);
      if (result.data?.success)  {
        setSuccess('You account is created successfully. Check your inbox to validate your email address.')
      }
    } catch(err) {
      console.log('register resulterr', err);
      if (err.response) {
        console.log(err.response?.data);
        setError(err.response?.data?.error_message || err.response?.data?.message || 'There was an error');
      } else if (err.request) {
        console.log(err.request)
        // client never received a response, or request never left
      } else {
        console.log(err)
        // anything else
      }
    }
  }

  return (
    <Layout>
      <div className="flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
          <div>
            <img className="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="Workflow" />
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
              Register a new account
            </h2>
            <p className="mt-2 text-center text-sm text-gray-600">
              Or&nbsp;
              <a href="/login" className="font-medium text-indigo-600 hover:text-indigo-500">
                login to an existing account
              </a>
            </p>
          </div>
          <form className="space-y-6 mt-8" onSubmit={handleRegister}>
            <div className="rounded-md shadow-sm -space-y-px">

              <div>
                <label htmlFor="username" className="sr-only">Username</label>
                <input id='username' type='text' required placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"/>
              </div>

              <div>
                <label htmlFor="email" className="sr-only">Email</label>
                <input id='email' type='email' required placeholder="Email address" value={email} onChange={(e) => setEmail(e.target.value)} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"/>
              </div>

              <div>
                <label htmlFor="password" className="sr-only">Password</label>
                <input id='password' name="password" type='password' placeholder="Password" autoComplete="current-password" required value={password} onChange={(e) => setPassword(e.target.value)} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"/>
              </div>
            </div>

            <div className="text-sm text-right mb-4">
              <a href="/forgot-password" className="font-medium text-indigo-600 hover:text-indigo-500">
                Forgot your password?
              </a>
            </div>

            {error && (
              <div role="alert" className="my-2">
                <div className="border border-red-400 rounded bg-red-100 px-4 py-3 text-red-700">
                  <div dangerouslySetInnerHTML={{__html: error}}/>
                </div>
              </div>
            )}

            <div>
              <button type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                  <svg className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fillRule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clipRule="evenodd" />
                  </svg>
                </span>
                Register
              </button>
            </div>
          </form>
        </div>
      </div>
      {success && (
        <div className="modal fixed w-full h-full top-0 left-0 flex items-center justify-center">
          <div className="modal-overlay absolute w-full h-full bg-white opacity-95"></div>
          <div className="modal-container fixed w-full h-full z-50 overflow-y-auto ">
            <div className="modal-content max-w-screen-md container mx-auto h-auto text-left p-4">
            <div className="pb-2 pt-20 text-center">
              <p className="text-2xl font-bold">Account created successfully</p>
              <p>{success}</p>
              <a href="/login" className="mt-8 group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Go to login</a>
            </div>
            </div>
          </div>
        </div>
      )}
    </Layout>
  )
}
