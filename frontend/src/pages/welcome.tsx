import { useSession, signOut } from 'next-auth/client'
import { useRouter } from 'next/router'
import { useEffect } from 'react'

export default function Welcome () {
	const [session, loading] = useSession()
	const router = useRouter()

	useEffect(() => {
    console.log('welcome session hook', loading, session)
		if(!loading && !session?.accessToken) {
      console.log('no accesstoken')
			// router.push('/login')
		}
	}, [loading, session])

	return (
		<div className="container">
			<h1>Welcome {session?.user.name}</h1>
			<button onClick={() => signOut()}>Log Out</button>
		</div>
	)
}
