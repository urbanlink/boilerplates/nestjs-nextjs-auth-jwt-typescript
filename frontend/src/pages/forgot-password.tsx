import Head from 'next/head'
import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { signIn, useSession } from 'next-auth/client'
import { Layout } from '../components/Layout'

export default function ForgotPassword () {
  const [email, setEmail] = useState('')
  const [isLoginStarted, setIsLoginStarted] = useState(false)
  const [loginError, setLoginError] = useState('')
  const router = useRouter()

  useEffect(() => {
    if (router.query.error) {
      setLoginError(router.query.error?.toString() || undefined)
      setEmail(router.query.email?.toString() || undefined)
    }
  }, [router])

  const handleLogin = (e) => {
    e.preventDefault()
    setIsLoginStarted(true)
    signIn('credentials',
      {
        email,
        callbackUrl: `${window.location.origin}/welcome`
      }
    )
  }

  return (
    <Layout>
      <div className="container">
        <div className="max-w-md mt-8 mx-auto">
        <h1 className="mb-8">Forgot your password</h1>
        <h2>Enter your email address to receive an email</h2>
          {loginError && <div className="bg-red-50 text-red-600 m-4 p-4">{loginError}</div>}
          <form onSubmit={(e) => handleLogin(e)}>
            <div className="block mb-4">
              {/* <label classNam e="block font-bold" htmlFor='loginEmail'>Email</label> */}
              <input id='loginEmail' placeholder="Your email address" type='email' value={email} onChange={(e) => setEmail(e.target.value)} className="rounded border border-gray-400 px-2 py-1 w-full"/>
            </div>
          <button type='submit' className="rounded px-4 py-2 bg-gray-700 text-white hover:bg-gray-500" disabled={isLoginStarted}>Submit</button>
        </form>
        <a href="/login" className="my-8 block">Login</a>
        <a href="/home" className="my-8 block">Home</a>
        </div>
      </div>
    </Layout>
  )
}