
import Link from "next/link"
import { dependencies } from "../../../package.json"

export const Footer = () => {
  const items = [{
    title: 'About', 
    link: '/about'
  },
  {
    title: 'Gitlab', 
    link: 'https://gitlab.com/urbanlink/boilerplates/nestjs-nextjs-auth-jwt-typescript',
    external: true,
    icon: 'fa-gitlab'
  },
  {
    title: 'Policy',
    link: '/policy'
  }, {
    title: 'Contact',
    link: '/contact'
  }];

  return (
    <footer className="bg-gray-600 text-sm py-8">
      <div className="container">
        <ul className="">
          {items.map((item, index) => (
            <li className="inline" key={index}>
              <a href={item.link} target={item.external ? '_blank' : null} className="text-white mr-4">{item.icon && <i className="fa"></i>}{item.title}</a>
            </li>  
          ))}
        </ul>
      </div>
    </footer>
  )
}