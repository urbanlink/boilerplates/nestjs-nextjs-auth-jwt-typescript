# NextJS NestJS Boilerplate with JWT authentication

## References

### Docker

  * https://blog.logrocket.com/containerized-development-nestjs-docker/

### NestJS
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ff253d396e21c4c593e8)

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

Got to <a href="http://localhost:8000/swagger ">http://localhost:8000/swagger</a> to find the swagger doc.



  * https://github.com/jaygould/nextjs-typescript-jwt-boilerplate
  * https://github.com/NewSchoolApp/newschool-backend
  * https://gist.github.com/jengel3/6a49a25b2fc2eb56fcf8b38f5004ea2c
  * https://github.com/jengel3/nestjs-auth-example/tree/master/src
  * https://github.com/modernweb-pl/vue-nest-monorepo/tree/master/apps/api/src/auth
  * https://github.com/nestjs/docs.nestjs.com/blob/70ce6152e014b0bf2618b4759f3070b88f811090/content/techniques/authentication.md
  * https://github.com/modernweb-pl/vue-nest-monorepo/pull/32/files
  * https://github.com/nestjs/jwt/issues/122
  * https://www.codemag.com/Article/2001081/Nest.js-Step-by-Step-Part-3-Users-and-Authentication
  * https://javascript.plainenglish.io/nestjs-implementing-access-refresh-token-jwt-authentication-97a39e448007
  * https://github.com/abouroubi/nestjs-auth-jwt
  * 

### NextJS

  * https://dev.to/twisha/using-credentials-provider-with-a-custom-backend-in-nextauth-js-43k4
  * https://github.com/nextauthjs/next-auth-typescript-example/tree/main/pages
  * https://github.com/lawrencecchen/next-auth-refresh-tokens/blob/main/pages/index.js
  * https://next-auth.js.org/tutorials/refresh-token-rotation#client-side
  * https://next-auth.js.org/configuration/events
  * https://next-auth.js.org/providers/credentials
  * https://github.com/nextauthjs/next-auth/issues/1391
  * https://github.com/nextauthjs/next-auth/discussions/1732
  * https://github.com/jaygould/nextjs-typescript-jwt-boilerplate/tree/master/api/src
  * https://www.jaygould.co.uk/2020-01-31-nextjs-auth-jwt-context-hooks/